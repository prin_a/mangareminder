package singleton;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import items.Manga;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Estgame on 6/4/2016.
 */
public class Server {

    private OkHttpClient client = new OkHttpClient();
    private List<String> allnames = new ArrayList<String>();
    private List<Manga> allManga = new ArrayList<Manga>();

    private static Server server = new Server();

    private Server(){}

    public static Server getInstance() {return server;}

    public String getData(String url){
        try {
            return new Updater().execute(url).get().toString();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class Updater extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String url = params[0].toString();
            try {
                return run(url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        public String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public List<Manga> getFavoriteManga(InputStream in, List<Manga> favorites){
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder total = new StringBuilder();
            String line = "";
            String[] split;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
                split = line.split("\"");
                Manga m = new Manga(split[1],split[3]);
                favorites.add(m);
            }
            return favorites;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void initList(){
        String data = "";
        String url = "";
        String[] split;
        String[] inner;
        boolean keep = false;
        boolean header = true;
        for(int z = 1;z<=18;z++){
            keep = false;
            header = true;
            url = "http://www.niceoppai.net/manga_list/all/any/name-az/" + z + "/";
            data = getData(url);
            split = data.split("<");
            for(int i = 0; i<split.length;i++) {
                if (keep && !header) {
                    if (split[i].contains("href") && !split[i].contains("manga_list")) {
                        if(split[i].contains("facebook.com")) break;
                        inner = split[i].split("\"");
                        if(!inner[2].substring(1).equalsIgnoreCase("")) {
                            String name = inner[2].substring(1);
                            name = name.replaceAll("&amp;","&");
                            name = name.replaceAll("&#039;","'");
                            allManga.add(new Manga(name,inner[1]));
                            allnames.add(inner[2].substring(1));
                        }
                    }
                } else {
                    if (split[i].contains("name-az")) {
                        keep = true;
                    }
                    if(split[i].contains("toriko")){
                        header = false;
                    }
                }

            }

        }
    }

    public Manga getManga(String name){
        for (Manga m :allManga){
            if(m.getName().equalsIgnoreCase(name))
                return m;
        }
        return null;
    }

    public List<String> getAllnames() {
        return allnames;
    }

    public List<Manga> getAllManga() {
        return allManga;
    }
}
