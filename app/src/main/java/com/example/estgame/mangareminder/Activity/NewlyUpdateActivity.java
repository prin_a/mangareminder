 package com.example.estgame.mangareminder.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.estgame.mangareminder.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import items.Manga;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import singleton.Server;

 public class NewlyUpdateActivity extends AppCompatActivity {

     private ImageButton home;
     private LinearLayout layout;
     private List<Manga> mangas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newly_update);
        initComponent();
        getUpdate();
    }

    private void initComponent() {
        mangas =  new ArrayList<Manga>();
        home = (ImageButton) findViewById(R.id.homeButton);
        layout = (LinearLayout) findViewById(R.id.layout);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewlyUpdateActivity.this, MainActivity.class);
                startActivity(i);
            }
        });


    }


     private void getUpdate(){

         String a = Server.getInstance().getData("http://www.niceoppai.net/");
         String[] split = a.split("<");
         String[] inner;
         String url = "";
         String[] checker;
         String same = "";
         String b = "";
         boolean keep = false;
         boolean skip = false;
         for(int i = 0; i<split.length;i++){
             if(keep) {
                 if (split[i].contains("href")){
                     if(split[i].contains("latest-chapters/1")){
                         break;
                     }
                     if(split[i].contains("lst")) {
                         inner = split[i].split("\"");
                         for(int j = 0;j<inner.length;j++){
                             if(inner[j].contains("http")){
                                 url = inner[j];
                                 if(same.equalsIgnoreCase("")){
                                     checker = inner[j].split("/");
                                     same = checker[3];
                                     b+="\n" + url;
                                 } else {
                                     checker = inner[j].split("/");
                                     if(same.equalsIgnoreCase(checker[3])) {
                                         skip = true;
                                     }
                                     else {
                                         skip = false;
                                         same = checker[3];
                                         b += "\n" + url;
                                     }
                                 }
                             } else if(inner[j].contains("อ่านการ์ตูน") && !skip ){
                                 mangas.add(new Manga(inner[j].substring(12),url));
                                 b+="\n" + inner[j].substring(12);
                             }
                         }
                     }

                 }

             } else {
                 if(split[i].contains("การ์ตูนที่อัพเดทแต่ละตอน")){
                     keep = true;
                 }
             }
         }



         initUpdateManga();

     }

     private void initUpdateManga(){
         for (final Manga m: mangas){
             Button button = new Button(layout.getContext());
             button.setBackgroundColor(Color.TRANSPARENT);
             button.setText(m.getName());
             button.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     if (isInternetconnected()) {
                         openWebsite(m.getLink());
                     } else {
                         AlertDialog alert = new AlertDialog.Builder(NewlyUpdateActivity.this).create();
                         alert.setTitle("Error");

                         alert.setMessage("No internet connected. Please check your connection.");
                         alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                 new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int which) {
                                         dialog.dismiss();
                                     }
                                 });
                         alert.show();
                     }
                 }
             });
             layout.addView(button);
         }
     }

     private void openWebsite(String url){
         Uri uri = Uri.parse(url);
         Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
         startActivity(launchBrowser);
     }

     public boolean isInternetconnected() {
         ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
         return cm.getActiveNetworkInfo() != null;
     }



}
