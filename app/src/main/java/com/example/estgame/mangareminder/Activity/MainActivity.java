package com.example.estgame.mangareminder.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.estgame.mangareminder.R;

import java.io.IOException;

import singleton.Server;

public class MainActivity extends Activity {

    private ImageButton favoriteButton;
    private ImageButton newlyUpdateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponent();
    }

    private void initComponent() {
        favoriteButton = (ImageButton) findViewById(R.id.favoriteList);
        newlyUpdateButton = (ImageButton) findViewById(R.id.newlyUpdate);

        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, FavoriteListActivity.class);
                startActivity(i);
            }
        });
        newlyUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetconnected()){
                    Intent i = new Intent(MainActivity.this, NewlyUpdateActivity.class);
                    startActivity(i);
                } else {
                    AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
                    alert.setTitle("Error");

                    alert.setMessage("No internet connected. Please check your connection.");
                    alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alert.show();
                }
            }
        });
    }

    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
