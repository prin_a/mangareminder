package com.example.estgame.mangareminder.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.estgame.mangareminder.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import items.Manga;
import singleton.Server;

public class FavoriteListActivity extends AppCompatActivity {

    private ImageButton select;
    private ImageButton remove;
    private ImageButton add;
    private ImageButton home;
    private LinearLayout layout;
    private AutoCompleteTextView autoText;
    private int index = -1;

    private List<Manga> favoriteList;
    private List<Button> buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);

        initComponent();
    }

    private void initComponent(){
        favoriteList = new ArrayList<Manga>();
        buttons = new ArrayList<Button>();
        select = (ImageButton) findViewById(R.id.selectButton);
        remove = (ImageButton) findViewById(R.id.removeButton);
        add = (ImageButton) findViewById(R.id.addButton);
        home = (ImageButton) findViewById(R.id.homeButton);
        layout = (LinearLayout) findViewById(R.id.layout);
        autoText = (AutoCompleteTextView) findViewById(R.id.autoText);



        try {
            favoriteList = Server.getInstance().getFavoriteManga(openFileInput("favorites.txt"),favoriteList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] mangalist = new String[Server.getInstance().getAllnames().size()] ;
        mangalist = (String[]) Server.getInstance().getAllnames().toArray(mangalist);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,mangalist);
        autoText.setAdapter(adapter);

        initFavoriteManga();

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index==-1) return;
                if (isInternetconnected()) {
                    openWebsite(favoriteList.get(index).getLink());
                } else {
                    AlertDialog alert = new AlertDialog.Builder(FavoriteListActivity.this).create();
                    alert.setTitle("Error");

                    alert.setMessage("No internet connected. Please check your connection.");
                    alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alert.show();
                }
            }
        });

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index == -1) return;
                layout.removeViewAt(index);
                favoriteList.remove(index);
                buttons.remove(index);
                index = -1;
                saveFile();
                initFavoriteManga();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = String.valueOf(autoText.getText());
                for(Manga favorite: favoriteList){
                    if(favorite.getName().equalsIgnoreCase(s)){
                        return;
                    }
                }
                Manga m = Server.getInstance().getManga(s);
                if(m != null) {
                    favoriteList.add(m);
                    saveFile();
                    initFavoriteManga();
                } else {
                    AlertDialog alert = new AlertDialog.Builder(FavoriteListActivity.this).create();
                    alert.setTitle("Error");

                    alert.setMessage("Please input manga in the list.");
                    alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alert.show();
                }
                autoText.setText("");
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FavoriteListActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void initFavoriteManga(){
        buttons.clear();
        layout.removeAllViews();
        for(final Manga m: favoriteList){
            Button button = new Button(layout.getContext());
            button.setBackgroundColor(Color.TRANSPARENT);
            button.setText(m.getName());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (index == buttons.indexOf(v)) {
                        if (isInternetconnected()) {
                            openWebsite(m.getLink());
                        } else {
                            AlertDialog alert = new AlertDialog.Builder(FavoriteListActivity.this).create();
                            alert.setTitle("Error");

                            alert.setMessage("No internet connected. Please check your connection.");
                            alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alert.show();
                        }
                    } else {
                        if (index == -1) {
                            index = buttons.indexOf(v);
                            v.setBackgroundColor(Color.BLUE);
                        } else {
                            buttons.get(index).setBackgroundColor(Color.TRANSPARENT);
                            index = buttons.indexOf(v);
                            v.setBackgroundColor(Color.BLUE);
                        }
                    }

                }
            });
            layout.addView(button);
            buttons.add(button);
        }
    }

    public void saveFile(){
        String total = "";
        try {
            for(int i = 0;i<favoriteList.size();i++){
                total += favoriteList.get(i).toString();
                if(i!=favoriteList.size()-1) total += "\n";
            }
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(FavoriteListActivity.this.openFileOutput("favorites.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(total);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openWebsite(String url){
        Uri uri = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(launchBrowser);
    }

    public boolean isInternetconnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

}
