package items;

/**
 * Created by Estgame on 6/4/2016.
 */
public class Manga {
    private String name = "";
    private String link = "";

    public Manga(String name){
        this.name = name;
    }

    public Manga(String name,String link){
        this.name = name;
        this.link = link;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }



    @Override
    public String toString() {
        return "title=\"" + name + "\" href=\""+link+"\"";
    }
}
